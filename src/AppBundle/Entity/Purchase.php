<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Purchase
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PurchaseRepository")
 */
class Purchase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="timestampDateStart", type="integer")
     */
    private $timestampDateStart;

    /**
     * @var integer
     *
     * @ORM\Column(name="itemId", type="integer")
     */
    private $itemId;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="count", type="integer")
     */
    private $count;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timestampDateStart
     *
     * @param integer $timestampDateStart
     * @return Purchase
     */
    public function setTimestampDateStart($timestampDateStart)
    {
        $this->timestampDateStart = $timestampDateStart;

        return $this;
    }

    /**
     * Get timestampDateStart
     *
     * @return integer 
     */
    public function getTimestampDateStart()
    {
        return $this->timestampDateStart;
    }

    /**
     * Set itemId
     *
     * @param integer $itemId
     * @return Purchase
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Get itemId
     *
     * @return integer 
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return Purchase
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set count
     *
     * @param integer $count
     * @return Purchase
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer 
     */
    public function getCount()
    {
        return $this->count;
    }
}
