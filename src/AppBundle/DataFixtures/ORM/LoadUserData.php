<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Purchase;

class LoadUserData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 100; $i++) {
            $dateTime = new \DateTime();
            $dateTime->setTime(0, 0, 0);
            for ($j = 0; $j < 50; $j++) {
                $daysBefore = rand(1, 2);
                $selectedDate = $dateTime->modify("-{$daysBefore} day");
                $userAdmin = new Purchase();
                $userAdmin->setTimestampDateStart($selectedDate->getTimestamp());
                $userAdmin->setCount(rand(0, 20000));
                $userAdmin->setItemId($i);
                $userAdmin->setPrice(rand(100, 2000));
                $manager->persist($userAdmin);
            }
            $manager->flush();
        }
    }
}