<?php

namespace AppBundle\Controller;

use AppBundle\Form\ReportType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/report/form", name="form_report")
     * @Template()
     * @param Request $request
     * @return mixed
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(new ReportType());
        if ($request->getMethod() === "POST") {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $dateStart = $data['dateStart']->setTime(0, 0, 0);
                $dateEnd = $data['dateEnd']->setTime(0, 0, 0);
                return $this->redirectToRoute("grid_report", array('dateStart' => $dateStart->format("d-m-Y"), 'dateEnd' => $dateEnd->format('d-m-Y')));
            }
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/report/grid/{dateStart}/{dateEnd}", name="grid_report")
     * @Template()
     * @param string $dateStart
     * @param string $dateEnd
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reportAction($dateStart, $dateEnd)
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $dateStart = new \DateTime($dateStart);
        $dateEnd = new \DateTime($dateEnd);
        $purchases = $this->get('doctrine')->getRepository('AppBundle:Purchase')->getItemsInPeriod($dateStart, $dateEnd);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $purchases,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        $datesArray = $this->get('app.report_manager')->getDatesArray($dateStart, $dateEnd);

        return array('pagination' => $pagination, 'dates' => $datesArray);
    }

}
