<?php

namespace AppBundle\Manager;

use Symfony\Component\DependencyInjection\Container;

class ReportManager
{
    protected $container;

    function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getDatesArray(\DateTime $dateStart, \DateTime $dateEnd)
    {
        $datesArray = [];
        while ($dateStart < $dateEnd) {
            $dateStart->format('d-m-Y');
            $dateStart->modify('+1 day');
            array_push($datesArray, $dateStart->format('d-m-Y'));
        }

        return $datesArray;
    }

//    public function getReport(\DateTime $dateStart, \DateTime $dateEnd)
//    {
//        $purchases = $this->container->get('doctrine')->getRepository('AppBundle:Purchase')->getItemsInPeriod($dateStart, $dateEnd);
//        $dateCounter = clone $dateStart;
//        $report = [];
//        foreach ($purchases as $purchase) {
//            while ($dateCounter < $dateEnd) {
//                $report['date'] = $dateCounter->format('d-m-Y');
//                $report['date'] = $dateCounter->format('d-m-Y');
//
//                $dateCounter->modify('+1 day');
//            }
//        }
//
//
//        return $qb->getQuery();
//    }
}